package com.meriler.utils.dialogutilities

import android.app.Activity
import android.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_input.view.*

fun Activity.showInputDialog(titleAndHint: String, defaultText: String, action: (String) -> Unit){
    val dialog = with(AlertDialog.Builder(this)){
        val view = layoutInflater.inflate(R.layout.dialog_input, null, false)
        val input = view.dialoginput_input
        input.setText(defaultText)

        setTitle(titleAndHint)
        setView(view)
        setPositiveButton("Ok"){ _, _ ->
            action(input.text.toString())
        }
        setNegativeButton("Cancel", null)
        create()
    }
    dialog.show()
}