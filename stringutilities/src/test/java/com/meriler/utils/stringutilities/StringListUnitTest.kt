package com.meriler.utils.stringutilities

import org.junit.Assert.*
import org.junit.Test

class StringListUnitTest {
    @Test
    fun stringList_ToString() {
        val strings = arrayOf("Hi", "There", "Test", "String")
        val stringList = StringList(elements = *strings)
        val result = stringList.toString()
        print("Creation result: $result")
        assertEquals("Hi,There,Test,String", result)
    }

    @Test
    fun stringList_Adding(){
        val stringList = StringList()
        stringList.addAll(arrayOf("Test", "Moar"))
        val result = stringList.toString()
        print(result)
        assertEquals("Test,Moar", result)
    }

    @Test
    fun stringList_Removing(){
        val strings = arrayOf("Hi", "There", "Test", "String")
        val stringList = StringList("Hi", "There", "Test", "String")
        stringList.remove("Hi")
        val result = stringList.toString()
        print(result)
        assertEquals("There,Test,String", result)
    }

    fun stringList_FromString(){
        val strings = arrayOf("Hi", "There", "Test", "String")
        val stringList = StringList("Hi", "There", "Test", "String")
        stringList.remove("Hi")
        val result = stringList.toString()
        print(result)
        assertEquals("There,Test,String", result)
    }
}