package com.meriler.utils.stringutilities

fun ByteArray.toHexString(): String{
    var string = "0x"
    for (b in this) {
        string += String.format("%02X", b)
    }
    return string
}