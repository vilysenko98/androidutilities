package com.meriler.utils.stringutilities

class StringList : ArrayList<String>{
    companion object {
        const val DEFAULT_SEPARATOR = ","
    }

    val separator: String
    //Inheritance ol super constructors
    constructor(separator: String = DEFAULT_SEPARATOR): super(){
        this.separator = separator
    }
    constructor(initialCapacity: Int, separator: String = DEFAULT_SEPARATOR): super(initialCapacity){
        this.separator = separator
    }
    constructor(collection: MutableCollection<out String>, separator: String = DEFAULT_SEPARATOR): super(collection){
        this.separator = separator
    }
    constructor(vararg elements: String, separator: String = DEFAULT_SEPARATOR): this(separator){
        if(elements.size > 0)
            addAll(elements)
    }
    constructor(vararg elements: String): this(*elements, separator = DEFAULT_SEPARATOR)

    fun fromString(string: String){
        clear()
        addAll(string.split(separator))
    }

    override fun toString(): String {
        return joinToString(separator)
    }
}