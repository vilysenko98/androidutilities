package com.meriler.utils.essentialsutilities

fun <T: Any?> checkAllNotNull(vararg elements: T): Boolean{
    elements.forEach {
        if(it == null)
            return false
    }
    return true
}

fun doAsync(run: () -> Unit){
    Thread{ run() }.start()
}
