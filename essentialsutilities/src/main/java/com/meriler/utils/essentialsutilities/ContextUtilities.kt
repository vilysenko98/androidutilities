package com.meriler.utils.essentialsutilities

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.annotation.StringRes
import android.widget.Toast

//region Activity start
inline fun <reified T: Activity> Context.startNewActivity(){
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T: Activity> Context.startNewActivity(vararg attributes: Pair<String, Any>){
    val intent = Intent(this, T::class.java)

    if(attributes.isNotEmpty()) {
        attributes.forEach { pair ->
            val value = pair.second

            when (value) {
                is Int -> intent.putExtra(pair.first, value)
                is String -> intent.putExtra(pair.first, value)
                is Long -> intent.putExtra(pair.first, value)
                is CharSequence -> intent.putExtra(pair.first, value)
                is Float -> intent.putExtra(pair.first, value)
                is Double -> intent.putExtra(pair.first, value)
                is Boolean -> intent.putExtra(pair.first, value)
                is Short -> intent.putExtra(pair.first, value)
                is Char -> intent.putExtra(pair.first, value)
            }
        }
    }

    startActivity(intent)
}
//endregion

//region Toasts display
fun Context.showToast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showToast(@StringRes message: Int){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showLongToast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.showLongToast(@StringRes message: Int){
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}
//endregion

//region Toast creation
fun Context.createToast(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT)
fun Context.createToast(@StringRes message: Int) = Toast.makeText(this, message, Toast.LENGTH_SHORT)
fun Context.createLongToast(message: String) = Toast.makeText(this, message, Toast.LENGTH_LONG)
fun Context.createLongToast(@StringRes message: Int) = Toast.makeText(this, message, Toast.LENGTH_LONG)
//endregion