package com.meriler.utils.essentialsutilities

import android.os.Handler

fun Handler.postDelayed(delayMillis: Long, runnable: () -> Unit) = postDelayed(runnable, delayMillis)