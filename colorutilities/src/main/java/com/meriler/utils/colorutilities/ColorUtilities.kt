package com.meriler.utils.colorutilities

import android.graphics.Color

val Int.isColorBright: Boolean
    get() {
        if (android.R.color.transparent == this)
            return true

        var rtnValue = false

        val rgb = intArrayOf(Color.red(this), Color.green(this), Color.blue(this))

        val brightness = Math.sqrt(
            rgb[0].toDouble() * rgb[0].toDouble() * .241 + (rgb[1].toDouble()
                    * rgb[1].toDouble() * .691) + rgb[2].toDouble() * rgb[2].toDouble() * .068
        ).toInt()

        // color is light
        if (brightness >= 200)
            rtnValue = true
        return rtnValue
    }