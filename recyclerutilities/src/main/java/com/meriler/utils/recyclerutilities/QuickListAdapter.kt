package com.meriler.utils

import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * Simplified class for creating basic android recycler adapter using few lines of code
 */
typealias DataBinder<T> = QuickListAdapter<T>.(item: T, view: View) -> Unit

class QuickListAdapter<T: Any>(
    @LayoutRes private val layout: Int,
    private val dataList: MutableList<T> = mutableListOf(),
    private var bindData: DataBinder<T>? = null
) : RecyclerView.Adapter<QuickListAdapter.ViewHolder>() {
    /**
     * Simpler constructor when you want to ignore data input on instance creation
     */
    constructor(@LayoutRes layout: Int, bindData: DataBinder<T>): this(layout, mutableListOf(), bindData)
    /**
     * Simpler constructor when you just want to show off the information with data providing on instance creation
     */
    constructor(dataList: MutableList<T>): this(android.R.layout.activity_list_item){
        bindData = { item, view ->
            view.findViewById<TextView>(android.R.id.text1).text = item.toString()
        }
    }
    /**
     * Simpler constructor when you just want to show off the information without providing any data on instance creation
     */
    constructor(): this(mutableListOf())

    /**
     * Data updater listener
     */
    private var mOnDataUpdated: ((List<T>) -> Unit)? = null

    //region List working methods
    /**
     * Adding to list
     */
    fun add(vararg items: T){
        dataList.addAll(items)
        notifyListUpdate()
    }

    /**
     * Removing from the list
     */
    fun remove(item: T): Boolean{
        if(dataList.remove(item)) {
            notifyListUpdate()
            return true
        }
        return false
    }

    /**
     * Clearing out the list
     */
    fun clear(){
        dataList.clear()
        notifyListUpdate()
    }

    /**
     * Getting the list's item value
     */
    operator fun get(index: Int) = dataList[index]

    /**
     * Setting list's time value
     */
    operator fun set(index: Int, value: T){
        dataList[index] = value
        notifyListUpdate()
    }

    fun indexOf(element: T) = dataList.indexOf(element)
    //endregion

    /**
     * Setting on data updated provider
     */
    fun setOnDataUpdatedListener(listener: (List<T>) -> Unit){
        mOnDataUpdated = listener
    }


    //region Generic recycler methods
    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bindData?.invoke(this, dataList[position], holder.itemView)
    }
    //endregion

    /**
     * To not rewrite 2 lines over and over
     */
    private fun notifyListUpdate(){
        notifyDataSetChanged()
        mOnDataUpdated?.invoke(dataList.toList())
    }


    class ViewHolder(itemId: View): RecyclerView.ViewHolder(itemId)
}